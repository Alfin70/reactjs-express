const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

const tasks = [
    {id: 1, title : 'Pergi ke pesta', slug : 'pergi ke pesta hari minggu jam 7'},
    {id: 2, title : 'beli belanja', slug : 'belanja ke luwes jam 5'},
    {id: 3, title : 'main dengan teman', slug : 'ngegame dota dengan teman jam 10'},
    {id: 4, title : 'ngedate', slug : 'ngedate with someone kyaaaa >.<'},
  
]

app.get('/', (req, res) => {
   
});

app.get('/api/tasks', (req, res) => {
    res.send(tasks);
});

app.get('/api/tasks/:id', (req, res) => {
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');
    res.send(task); 
});

app.post('/api/tasks', (req, res)=>{
    const {error} = validateTask(req.body);
    if (error) return res.status(400).send(result.error.details[0].message);
    

    const task ={
        id: tasks.length +1,
        title: req.body.title,
        slug: req.body.slug
    };
    tasks.push(task);
    res.send(task);
});

app.put('/api/tasks/:id', (req, res)=>{
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');
        
    

    const {error} = validateTask(req.body);
    if (error) return res.status(400).send(result.error.details[0].message);
        
    

    task.title = req.body.title;
    task.slug =  req.body.slug;
    res.send(task);

});

function validateTask(task){
    const schema = {
        title: Joi.string().min(3).required(),
        slug: Joi.string().min(5).required()
    };
    return Joi.validate(task, schema);
}

app.delete('/api/tasks/:id', (req, res)=>{
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');

    const index = tasks.indexOf(task);
    tasks.splice(index, 1);

    res.send(task);

});

const port = process.env.PORT || 3001
app.listen(port, ()=>console.log(`Listening on port ${port}...`));
