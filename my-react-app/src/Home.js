import React, { Component } from 'react';
import './App.css';
import { Button } from 'react-bootstrap';
import  axios from 'axios';

export class Home extends Component {
 
  state = {
    loading: true,
    tasks: [],
    title: null,
    slug: null,
  }
  async componentDidMount(){

    const url = "/api/tasks";
    const response = await fetch(url);
    const data = await response.json();
    this.setState({tasks: data, loading: false});
  } 

  saveDataToApi(e){
    e.preventDefault()
    const apiUrl = "http://localhost:3001/api/tasks";
    const data = {
      title: this.state.title,
      slug: this.state.slug,
    }
    axios.post(apiUrl, data)
    .catch(err => {
      console.log(err)
      console.log(err.message)
    })
  }
  render(){
    return(
      <div>
        {this.state.loading || !this.state.tasks ?
         (
         <div>loading....</div>
         ) : (
    
        <div className="d-flex align-content-stretch flex-wrap bd-highlight example-parent">
          {this.state.tasks.map(task =>
        <ul key={task.id}>
         <div>
          <div className="p-2 bd-highlight col-example">Title:</div>{task.title}
          <div className="p-2 bd-highlight col-example">Note:</div>{task.slug}
          <div className="p-2 bd-highlight col-example"><Button variant="danger">Delete</Button>
          &nbsp;
          <Button variant="warning" type="submit">Edit</Button></div></div>
            
       
        </ul>
        
      )}
        
        </div>
         ) }  <div> <div className="tiform">Input Your Activity </div> 
         
              <form className="form-add" method="post" onSubmit={ (e) => this.saveDataToApi (e) }>
                <label>Title:</label>
                <input
                 type="text"
                  name="title"
                  onChange={ (e) =>{
                    this.setState({title: e.target.value})
                  } }
                  />
                <label>Note:</label>
                <input 
                type="text" 
                name="slug" 
                onChange={ (e) =>{
                  this.setState({title: e.target.value})
                } }
                />
                <button className="but">ADD</button>
              </form>
         
         
         
         </div>
      </div>
    );
  }


}
